// alert("Hello");


// OBJECTS

/*
	- An object is a data type that is used to represent real world objects
	- It is a collection of related data and / or functionalities
	- Information stored in objects are represented in "key:value" pair
	- Different data types may be stored in an obhect
*/

// Creating Objects
/*
	There are two ways to create objects:
	1. Object initializers/ Literal Notation
	2. Construction Functions
*/

// Creating objects through object initialization / literal notation
/*
	- This creates / declares an object and also initializes / assigns its values upon creation.
	- It already has its keys (properties) and values such as name, color, weight, etc
	- Syntax:
		let / const objectName = {
			keyA: valueA,
			keyB: valueB,

		} 

*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers / literal notation");
console.log(cellphone);
console.log(typeof cellphone);
console.log("");


// Creating objects using constructor functions
/*
	- Creates a reusable function to create several objects that have the same data structure
	- Useful for creating multiple instances / copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct / unique identit of it
	- Syntax:
		function objectName(keyA, keyB) {
			this.keyA = keyA,
			this.keyB = keyB
		}
*/

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

function Student(firstName, lastName, yearOld, city) {
	this.name = firstName + " " + lastName;
	this.age = yearOld;
	this.residence = city
}

/*
	- This "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function
*/

// This is an instance of the Laptop Object
console.log('This is a unique instance of the Laptop Object using object constructor:')
let laptop_mica = new Laptop('Lenovo', 2008);
console.log(laptop_mica);


let laptop_eric = new Laptop('Dell', 1990);
console.log(laptop_eric);

console.log("This is a unique instance of the Student object");
let elaine = new Student('Elaine', 'SJ', 12, 'Mars');
console.log(elaine);

let jake = new Student('Jake', 'Lexter', 10, 'Sun');
console.log(jake);


/*
	- The "new" keyword creates an instance of an object

*/

console.log("Result from creating objects without the 'new' keyword");
let oldLaptop = Laptop("PORTAL R3E CCMC", 1980)
console.log(oldLaptop); // undefined - laptop function walang return statement and if gumgamait ng construction function, kailangan mag "new"

// Accessing Object Prorties
// There are two ways to access object properties

// Using the dot notation
console.log(laptop_mica.name);
console.log(elaine.name);


// Using the "square bracket notation"
console.log("Result from square bracket notation: " + laptop_mica['name']);
console.log("Result from square bracket notation: " + elaine['name']);


let studentLaptop = laptop_mica.name;
console.log(studentLaptop);

// Initializing / Adding / Delete / Reassigning Object Proportion
/*
	- Like any other variable in Javascript, objects may have their own properties initialized after the object was created / declared
	- This is useful for times when an object's properties are undetermined at the time of beginning
*/

// Empty Object
console.log("Empty 'car' object:");
let car = {};
console.log(car);
console.log("");


car.name = "Honda Civic";
console.log(car);

// Initializing / Adding object properties using dot notation
car.driver = "Mr. Bean"
console.log("Result from adding properties using dot notation");
console.log(car);


// Initializing / Adding object properties using bracket notation
car["manufacture date"] = 2019
console.log(car["Manufacture date"]); //undefined
console.log(car["manufactureDate"]); //undefined
console.log(car["manufacture date"]); //dapat same para ma access

// console.log(car.manufacture date);
console.log('Result from adding properties using square bracket notation');
console.log(car);

// We use the bracket notation to access property names with spaces
console.log("");

// Deleting Object Properties
delete car['manufacture date'];
console.log("Result from deleting properties:");
console.log(car);

// Reassign object properties
car.name = "Dodge Charger R/T";
console.log("Result from reassigning properties:");
console.log(car);

// Object Methods
/*
	- A method is a function
	- A method is afunction attached to an object as a property
	- A method is useful for creating object specific properties
	- Similar to functions of real world objects, methods are defined based on what an object is capable of doing.
*/

let person = {
	name: 'John',
	talk: function() {
		console.log("Hello, my name is" + this.name)
	}
}

console.log(person);
console.log("Result from object methods")
person.talk();

//
person.walk = function() {
	console.log(this.name + 'walked 25 steps forward.')
}
person.walk(); //can add new keys/properties na di nakalagay sa original object


// Methods are useful for creatin reusable functions that perform tasks related to objects

let friend = {
	firstName: 'Arjay',
	lastName: 'Dala',
	address: {
		city: "Etibak",
		country: 'New York'
	},
	emails: ['arjay@friendster.com', 'arjay@myspace.com'],
	introduce: function(classmate) {
					console.log('Hello, my name is ' + this.firstName + " " + this.lastName + " . Nice to meet you " + classmate);
	}
}
friend.introduce('Eric');
friend.introduce('Mica');
friend.introduce('John Daniel');


// Object Literal for Pokemon
let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(targetPokemon) {
		console.log(this.name + " tackled " + targetPokemon);
		console.log(targetPokemon + "'s health is reduced to " + targetPokemon + "'s health")
	},
	faint: function(){
		console.log("Pokemon fainted")
	}
}
console.log(myPokemon);
myPokemon.tackle('Charmander');
myPokemon.tackle('Arceus');
console.log("");


// Create pokemon characters using constructor function
function Pokemon(name, level) {
	this.name = name,
	this.level = level,
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name)
		console.log(target.name + "'s health is now reduced to" + target.health)
	};
	this.faint = function() {
		console.log(this.name + "fainted")
	}
}

let pikachu = new Pokemon('Pikachu', 16);
let ratata = new Pokemon('Ratata', 8);


pikachu.tackle(ratata);


































